import { StyleSheet} from "react-native";

export const styles:any = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    ...StyleSheet.absoluteFillObject,
  },
  loading: {
    width: '100%',
    flexDirection: 'column',
    flex : 1,
    alignItems: 'center',
    justifyContent:'flex-start',
  },
  button: {
    marginBottom: 30,
    width: '80%',
    alignItems: 'center',
    backgroundColor: '#2196F3',
    borderRadius: 5,
  },
  buttonText: {
    padding: 20,
    color: 'white',
    fontSize: 20,
  }
});