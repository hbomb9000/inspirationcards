import * as React from 'react';
import {
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import { Bubbles } from 'react-native-loader';

import ImageDisplay from './components/ImageDisplay';
import QuoteDisplay from './components/QuoteDisplay';

import { styles } from './inspiracards-style';

interface Props {}
interface State {
  photo?:any;
  quote?:any;
  photoLoaded?:boolean;
  quoteLoaded?:boolean;
}
export default class Inspiracards extends React.Component<Props, State> {
  constructor(props, state) {
    super(props, state);
    this.state = {
      photo: {},
      photoLoaded:false,
      quote: {},
      quoteLoaded:false,
    }

  }
  componentDidMount() {
    this.fetchImage();
    this.fetchQuote();
  }

  fetchQuote() {
    fetch('http://api.forismatic.com/api/1.0/?method=getQuote&key=123456&format=json&lang=en').then(response => {
      return response.json();
    }).catch(error => {
      console.log('ERROR ON QUOTE', error)
    }).then(resp => {
      if(resp){
        const quote = {
          quote: resp.quoteText,
          quoteLink: resp.quoteLink,
          quoted: resp.quoteAuthor,
        };
        this.setState({quote: quote});
        this.setState({quoteLoaded: true});
      }
    })
  }

  fetchImage() {
    fetch('https://api.unsplash.com/photos/random/?client_id=4a2839c4b755e85c08bed58e90e9969f19b91ec122e40b7d17fef5c705b841e6&').then(response => {
      if(response.status === 200) {
        return response.json();
      }
      throw new Error()
    }).catch(error => {
      console.log("ERROR ON PHOTO", error);
    }).then(resp => {
      if(resp) {
        const photo = {
          photographer: resp.user.name,
          photoLink: resp.user.links.html,
          photo: resp.urls.regular,
        };
        this.setState({photo: photo});
        this.setState({photoLoaded: true});
      } else {
        const photo = {
          photographer: 'Hal Leonard',
          photoLink: '#',
          photo: '/src/components/baseImage.png',
        };
        this.setState({photo: photo});
        this.setState({photoLoaded: true})
      }
    })
  }

  refreshQuoteAndPicture() {
    this.setState({quoteLoaded:false});
    this.setState({photoLoaded:false});
    this.fetchImage();
    this.fetchQuote();
  }
  renderImage() {
    if(this.state.photoLoaded) {
      return(
        <ImageDisplay photo={this.state.photo.photo} photographer={this.state.photo.photographer} photoLink={this.state.photo.photoLink}/>
      );
    }
    return(
      <View style={styles.loading}>
        <Bubbles size={10} color='#CCC'/>
        <Text>
          Loading Image
        </Text>
      </View>
    );
  }
  renderQuote() {
    if(this.state.quoteLoaded) {
      return(
        <QuoteDisplay quote={this.state.quote.quote} quoted={this.state.quote.quoted} quoteLink={this.state.quote.quoteLink}/>
      );
    }
    return(
      <View style={styles.loading}>
        <Bubbles size={10} color='#CCC'/>
        <Text>Loading Quote</Text>
      </View>
    );
  }
  render() {
    return (
      <View style={styles.container}>
        { this.renderImage() }
        { this.renderQuote() }
        <TouchableOpacity onPress={this.refreshQuoteAndPicture.bind(this)}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Show me another</Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}