import { StyleSheet} from "react-native";

export const styles:any = StyleSheet.create({
  quoteArea: {
    position: 'absolute',
    top: '20%',
    left: '10%',
    right: '10%',
    bottom: '20%',
  },
  quoteTextArea: {
    ...StyleSheet.absoluteFillObject,
  },
  quoteItself: {
    padding:20,
    ...StyleSheet.absoluteFillObject,
  },
  quoteBackground: {
    backgroundColor: '#86ffe4',
    borderWidth: 1,
    borderColor: '#86ffe4',
    borderRadius: 10,
    opacity: 0.55,
    width: '100%',
    height: '100%',
    ...StyleSheet.absoluteFillObject,
  },
  quoteText: {
    fontSize: 30,
    fontStyle: 'italic',
    fontFamily: 'serif',
    color: '#333333',
  },
  quoted: {
    textAlign: 'right',
    color: '#000000',
    fontStyle:'italic',
  }
});