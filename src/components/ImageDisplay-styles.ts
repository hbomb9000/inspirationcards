import { StyleSheet} from "react-native";

export const styles:any = StyleSheet.create({
  imageView: {
    ...StyleSheet.absoluteFillObject,
  },
  imageContainer: {
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    borderColor: '#FFFFFF',
    minHeight:'100%',
    minWidth:'100%',
    borderRadius: 10,
    opacity: 0.75,
  }
});