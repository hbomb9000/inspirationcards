import * as React from 'react';
import { Text, View } from 'react-native';
import { styles } from './QuoteDisplay-styles';


interface Props {
  quote:string;
  quoted:string;
  quoteLink:string;
}

interface State {

}
export default class ImageLoader<props, state> extends React.Component<Props, State> {
  render() {
    const quoted = (this.props.quoted === '') ? 'unknown': this.props.quoted;
    return(
      <View style={styles.quoteArea}>
        <View style={styles.quoteTextArea}>
          <View style={styles.quoteBackground}/>
          <View style={styles.quoteItself}>
            <Text style={styles.quoteText}>
              "{this.props.quote}"
            </Text>
            <View>
              <Text style={styles.quoted}>- {quoted}</Text>
            </View>
          </View>
        </View>
      </View>
    )
  }
}