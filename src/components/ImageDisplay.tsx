import * as React from 'react';
import { Image, Text, View, Linking } from 'react-native';
import { isString as _isString } from 'lodash';
import { styles } from './ImageDisplay-styles';
interface State {
  photo?:string;
  photographer?:string;
  photoLink?:string;
}

interface Props {
  photo?: string;
  photographer?: string;
  photoLink?: string;
  serviceName?: string;
  serviceLink?: string;
}
export default class ImageDisplay extends React.Component<Props, State> {

  getValidString(toCheck:string):string {
    return _isString(toCheck) ? toCheck: '';
  }
  displayImage(path:string):any {
    const checkedString = this.getValidString(path);
    if(checkedString.indexOf('http') > -1) {
      return(<Image style={styles.imageContainer} source={{uri:checkedString}}/>);
    } else {
      return(<Image style={styles.imageContainer} source={{uri:checkedString}}/>);
    }
  }
  getUtmSourceUrl(url:string): string {
    return `${this.getValidString(url)}/utm-source=inspirationalQuotes&utm-medium=referral`;
  }
  render() {
    const photographer:string = this.getValidString(this.props.photographer);
    const photoLink:string = this.getUtmSourceUrl(this.props.photoLink);
    return(
      <View style={styles.imageView}>
        {this.displayImage(this.props.photo)}
        <Text style={styles.imageText}>
          Photo by <Text style={styles.link} onPress={() => Linking.openURL(photoLink)}> {photographer} </Text> on <Text style={styles.link} onPress={() => Linking.openURL(this.getUtmSourceUrl('https://unsplash.com'))}>Unsplash</Text>.
        </Text>
      </View>
    )
  }
}